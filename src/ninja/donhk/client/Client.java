package ninja.donhk.client;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

public class Client {

    private final String address;
    private final int port;

    public Client(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public void connect() throws IOException, InterruptedException {
        System.err.println("Connecting client to server");
        final Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(address, port), 5000);
        } catch (ConnectException | SocketTimeoutException e) {
            System.err.printf("Client was not able to connect to %s:%d", address, port);
            return;
        }

        final InputStream is = socket.getInputStream();
        final OutputStream os = socket.getOutputStream();

        final BufferedReader stdout = new BufferedReader(new InputStreamReader(is));
        final PrintWriter stdin = new PrintWriter(os, true);

        processQuestions(stdout, stdin);
    }

    private void processQuestions(BufferedReader stdout, PrintWriter stdin) throws InterruptedException, IOException {
        final String question1 = "question 1";
        System.err.printf("Asking '%s'\n", question1);
        stdin.println(question1);
        final String question1Answer = stdout.readLine();
        System.err.printf("%s -> %s\n", question1, question1Answer);

        TimeUnit.SECONDS.sleep(5);

        final String question2 = "question 2";
        System.err.printf("Asking '%s'\n", question2);
        stdin.println(question2);
        final String question2Answer = stdout.readLine();
        System.err.printf("%s -> %s\n", question2, question2Answer);

        TimeUnit.SECONDS.sleep(5);

        final String question3 = "question 3";
        System.err.printf("Asking '%s'\n", question3);
        stdin.println(question3);
        final String question3Answer = stdout.readLine();
        System.err.printf("%s -> %s\n", question3, question3Answer);

        TimeUnit.SECONDS.sleep(5);

        final String questionX = "question X";
        System.err.printf("Asking '%s'\n", questionX);
        stdin.println(questionX);
        final String questionXAnswer = stdout.readLine();
        System.err.printf("%s -> %s\n", questionX, questionXAnswer);
    }
}
