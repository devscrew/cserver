package ninja.donhk.server;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final int port;

    public Server(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        final ServerSocket server = new ServerSocket(port);
        System.out.println("Server is listening clients");
        System.out.printf("address: %s port %d \n", InetAddress.getLocalHost().getHostAddress(), port);

        final Socket client = server.accept();
        System.out.println("New client!");

        final InputStream is = client.getInputStream();
        final OutputStream os = client.getOutputStream();

        //preparing streams
        final BufferedReader stdout = new BufferedReader(new InputStreamReader(is));
        final PrintWriter stdin = new PrintWriter(os,true);

        dispatchQuestion(stdout, stdin);

        stdin.close();
        stdin.close();
    }


    private void dispatchQuestion(BufferedReader stdout, PrintWriter stdin) throws IOException {
        //reading from client
        System.out.println("\nReading client's question #1");
        final String question1 = stdout.readLine();
        System.out.printf("client says: '%s'\n", question1);
        questionOptions(question1, stdin);

        //reading from client
        System.out.println("Waiting for client's question #2");
        final String question2 = stdout.readLine();
        System.out.printf("client says: '%s'\n", question2);
        questionOptions(question2, stdin);

        //reading from client
        System.out.println("Waiting for client's question #3");
        final String question3 = stdout.readLine();
        System.out.printf("client says: '%s'\n", question3);
        questionOptions(question3, stdin);

        //reading from client
        System.out.println("Waiting for client's question X");
        final String questionX = stdout.readLine();
        System.out.printf("client says: '%s'\n", questionX);
        questionOptions(questionX, stdin);

    }


    private void questionOptions(String question, PrintWriter stdin) {
        if (question == null) {
            throw new IllegalArgumentException("Question is null!");
        }

        switch (question) {
            case "question 1":
                stdin.println("answer to q1");
                break;
            case "question 2":
                stdin.println("answer to q2");
                break;
            case "question 3":
                stdin.println("answer to q3");
                break;
            default:
                stdin.println("I don't know your question :(");
                break;
        }
    }
}
