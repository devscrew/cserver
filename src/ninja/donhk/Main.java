package ninja.donhk;

import ninja.donhk.client.Client;
import ninja.donhk.server.Server;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        final int port = 8089;

        new Thread(() -> {
            Server server = new Server(port);
            try {
                server.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        TimeUnit.SECONDS.sleep(3);

        new Thread(() -> {
            Client client = new Client("127.0.0.1", port);
            try {
                client.connect();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
